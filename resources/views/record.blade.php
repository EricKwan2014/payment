@extends('layouts.app')

@section('content')
    @if (!empty($user))
    	<div class="container">
	        <div class="row">
	            <div class="col-md-8 col-md-offset-2">
	                <div class="panel panel-default">
	                    <div class="panel-heading">Payment Record</div>
	                    <div class="panel-body">
	                        <label>Customer Name:</label>
	                        {{ $user->name }} <br />
	                        <label>Customer Phone Number:</label>
	                        {{ $user->phone }} <br />
	                        <label>Currency:</label>
	                        {{ $user->currency }} <br />
	                        <label>Price:</label>
	                        {{ $user->price }} <br />
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    @else
	    <record></record>
    @endif
@endsection