<?php

namespace App\Http\Controllers;

use App\User;
use Braintree_Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PaymentController extends Controller
{
    public function show()
    {
        return view('payment');
    }

    public function pay(Request $request)
    {
    	$amount = currency($request->price, $request->currency, config('currency.default'), false);
    	$sale = Braintree_Transaction::sale([
		    'amount' => number_format($amount, 2, '.', ''),
		    'paymentMethodNonce' => $request->payment_method_nonce,
		    'creditCard' => [
		    	'cardholderName' => $request->cardholder,
		    ],
		    'options' => [
		        'submitForSettlement' => true,
		        'storeInVaultOnSuccess' => true,
		    ]
		]);
		if ($sale->success) {
			$customer = new User;
			$customer->name = $request->name;
			$customer->phone = $request->phone;
			$customer->currency = $request->currency;
			$customer->price = $request->price;
			$customer->transaction_id = $sale->transaction->id;
			$customer->save();

			Cache::forever($request->name . ':' . $sale->transaction->id, $customer);

			return redirect()->back()->with([
	            'sweet_alert.title' => 'Success',
	            'sweet_alert.desc' => 'Please save your payment reference code: ' . $sale->transaction->id,
	            'sweet_alert.type' => 'success',
	        ]);
		}
		if ($sale->errors) {
			$errors = $sale->errors->deepAll();
			return redirect()->back()->with([
	            'sweet_alert.title' => 'Error',
	            'sweet_alert.desc' => $errors[0]->message,
	            'sweet_alert.type' => 'error',
	        ]);
		}
    }

    public function show_record(Request $request)
    {
    	return view('record');
    }

    public function record(Request $request)
    {
    	$user = Cache::get($request->name . ':' . $request->ref);

    	if ($user) {
    		return view('record', compact('user'));
	    } else {
	    	$db_user = User::where('transaction_id', $request->ref)->where('name', $request->name)->first();
	    	if ($db_user) {
	    		return view('record', compact('db_user'));
	    	}
	    	return redirect()->back()->with([
	            'sweet_alert.title' => 'Error',
	            'sweet_alert.desc' => 'Not Found',
	            'sweet_alert.type' => 'error',
	        ]);
	    }
	}
}
