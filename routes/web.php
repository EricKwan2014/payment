<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PaymentController@show');
Route::post('pay', 'PaymentController@pay');
Route::get('record', 'PaymentController@show_record');
Route::post('record', 'PaymentController@record');


/*
|--------------------------------------------------------------------------
| Braintree Token Controller
|--------------------------------------------------------------------------
|
| Noted: In the real world, this route need to more secure 
| Currently for demo purpose.
|
*/
Route::get('braintree/token', 'BraintreeTokenController@token');