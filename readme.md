## Usage

### Step 1: Git clone the repo.

```
git clone https://EricKwan2014@bitbucket.org/EricKwan2014/payment.git
```

### Step 2: Composer install and core setup

```
composer install
```

### Step 3: Install the dependencies

```
npm install
```

### Step 4: Setup environment

This will copy the example env setting for you, you are free to modify it!!!
```
cp .env.example .env
```
Also visit the config folder to change some setting to fullfill your development environment :)

### Step 5: Run npm run

This will run the command from package.json to compiled some frontend components
```
npm run dev
```

### Step 6: Run database

This will setup the server
```
touch database/database.sqlite
```

Then
```
php artisan migrate
```

### Step 7: Run currency setting

This will setup the currency for you and generally should add from a currency_file. 

```
php artisan currency:manage add 'HKD','USD','AUD','EUR','JPY','CNY'
```

By default exchange rates are updated from Finance Yahoo.com.
```
php artisan currency:update
```

### Finally Step: Serve it

```
php artisan serve
```
